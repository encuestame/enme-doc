YUI.add("yuidoc-meta", function(Y) {
   Y.YUIDoc = { meta: {
    "classes": [
        "Notifications.Activity",
        "Widget.Comment",
        "Widget.Comments",
        "Widget.DasboardWrapper",
        "Widget.EnmeMainLayoutWidget",
        "Widget.FolderSelect",
        "Widget.Message",
        "Widget.Notification",
        "Widget.NotificationItem",
        "Widget.NotificationList",
        "Widget.NotificationListItem",
        "Widget.Poll",
        "Widget.PublishSupport",
        "Widget.Suggest",
        "Widget.Table",
        "Widget.TableRow",
        "Widget.Toaster",
        "Widget.TweetPollList",
        "Widget.User",
        "Widget.UserGroup",
        "Widget.UserPermission",
        "Widget.UserTableRow",
        "Widget.WidgetServices",
        "Widget.enme",
        "Widgets.AbstractValidator",
        "Widgets.Answer",
        "Widgets.AnswerItem",
        "Widgets.EmailValidator",
        "Widgets.Hashtag",
        "Widgets.HashtagItem",
        "Widgets.HelpContextual",
        "Widgets.PasswordValidator",
        "Widgets.PublishSocialStatus",
        "Widgets.RealNameValidator",
        "Widgets.SignUp",
        "Widgets.SuggestItem",
        "Widgets.TweetPoll",
        "Widgets.TweetPollPublishInfo",
        "Widgets.TweetpollListDetail",
        "Widgets.UsernameValidator"
    ],
    "modules": [
        "Activity",
        "Admon",
        "Comment",
        "Dashboard",
        "Data",
        "ENME",
        "Folders",
        "Notifications",
        "Poll",
        "Publish",
        "Services",
        "SignUp",
        "Suggest",
        "Suggestion",
        "TweetPoll",
        "TweetPoll.Answer",
        "TweetPoll.AnswerItem",
        "TweetPoll.Hashtag",
        "TweetPoll.HashtagItem",
        "Tweetpoll",
        "TweetpollListDetail",
        "UI"
    ],
    "allModules": [
        {
            "displayName": "Activity",
            "name": "Activity"
        },
        {
            "displayName": "Admon",
            "name": "Admon"
        },
        {
            "displayName": "Comment",
            "name": "Comment"
        },
        {
            "displayName": "Dashboard",
            "name": "Dashboard"
        },
        {
            "displayName": "Data",
            "name": "Data"
        },
        {
            "displayName": "ENME",
            "name": "ENME"
        },
        {
            "displayName": "Folders",
            "name": "Folders"
        },
        {
            "displayName": "Notifications",
            "name": "Notifications"
        },
        {
            "displayName": "Poll",
            "name": "Poll"
        },
        {
            "displayName": "Publish",
            "name": "Publish"
        },
        {
            "displayName": "Services",
            "name": "Services"
        },
        {
            "displayName": "SignUp",
            "name": "SignUp"
        },
        {
            "displayName": "Suggest",
            "name": "Suggest"
        },
        {
            "displayName": "Suggestion",
            "name": "Suggestion"
        },
        {
            "displayName": "TweetPoll",
            "name": "TweetPoll"
        },
        {
            "displayName": "Tweetpoll",
            "name": "Tweetpoll"
        },
        {
            "displayName": "TweetPoll.Answer",
            "name": "TweetPoll.Answer"
        },
        {
            "displayName": "TweetPoll.AnswerItem",
            "name": "TweetPoll.AnswerItem"
        },
        {
            "displayName": "TweetPoll.Hashtag",
            "name": "TweetPoll.Hashtag"
        },
        {
            "displayName": "TweetPoll.HashtagItem",
            "name": "TweetPoll.HashtagItem"
        },
        {
            "displayName": "TweetpollListDetail",
            "name": "TweetpollListDetail"
        },
        {
            "displayName": "UI",
            "name": "UI"
        }
    ]
} };
});